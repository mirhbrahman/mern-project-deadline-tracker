import mongoose from "mongoose"
import bcrypt from 'bcryptjs'
import { v4 as uuidv4 } from 'uuid';

const userSchema = mongoose.Schema({
    business_id: {
        type: String,
        required: true,
        default: uuidv4()
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    designation: {
        type: String
    },
    userType: {
        type: String,
        required: true,
        default: 'main-user'
    }
},{
    timestamps: true
})

// Method for match password
userSchema.methods.matchPassword = async function (enteredPassword) {
    return await bcrypt.compare(enteredPassword, this.password)
}

// Hash password
userSchema.pre('save', async function(next){
    if (!this.isModified('password')){
        next()
    }

    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt)

})

const User = mongoose.model('User', userSchema)

export default User