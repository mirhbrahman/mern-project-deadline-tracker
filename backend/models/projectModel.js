import mongoose from "mongoose"

const projectSchema = mongoose.Schema({
    business_id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    clientName: {
        type: String,
        default: ''
    },
    shortDesc: {
        type: String,
        maxLength: 300,
        default: ''
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    status: {
        type: String,
        required: true,
        default: 'pending'
    },
    teams: [
        {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Team',
        }
    ],
    members: [
        {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User',
        }
    ],
}, {
    timestamps: true
})

const Project = mongoose.model('Project', projectSchema)

export default Project