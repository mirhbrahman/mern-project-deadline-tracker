import mongoose from "mongoose"

const teamSchema = mongoose.Schema({
    business_id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    members: [
        {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User',
        }
    ],
}, {
    timestamps: true
})

const Team = mongoose.model('Team', teamSchema)

export default Team