import express from 'express'
import dotenv from 'dotenv'
import cors from 'cors'

import authRoute from './routes/authRoutes.js'
import employeeRoute from './routes/employeeRoutes.js'
import teamRoute from './routes/teamRoutes.js'
import projectRoute from './routes/projectRoutes.js'

import connectDB from './config/db.js'
import {notFound, errorHandler} from './middleware/errorMiddleware.js'

// App
const app = express()
dotenv.config()
connectDB()

app.use(express.json())
app.use(cors())

// Load route
app.use('/api/auth', authRoute)
app.use('/api/employees', employeeRoute)
app.use('/api/teams', teamRoute)
app.use('/api/projects', projectRoute)

app.get('/', function(req, res){
    res.send('ddd')
})


// Handle error
app.use(notFound)
app.use(errorHandler)

// Run server
const PORT = process.env.PORT || 5000

app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`))