const generateErrorList = (error) => {
    let errors = {};
   
    if (error) {
        // Set all errors
        error.details.map(err => {
            errors[err.path] = err.message
        });
    } else {
        errors = false;
    }

    return {errors};
}

export default generateErrorList