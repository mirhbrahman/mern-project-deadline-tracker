import asyncHandler from 'express-async-handler'

import { projectCreateValidate, projectUpdateValidate } from '../validations/project.js'
import Project from '../models/projectModel.js'

// Create project
const createProject = asyncHandler(async (req, res) => {
    // Check for validation
    const { errors } = projectCreateValidate(req.body)
    if (errors) return res.status(400).json(errors)

    const { name, clientName, shortDesc, startDate, endDate, status,teams, members } = req.body

    const projectExist = await Project.findOne({ name })

    if (projectExist) {
        res.status(400)
        throw new Error('Project already exist')
    }

    const project = await Project.create({
        business_id: req.user.business_id,
        name,
        clientName,
        shortDesc,
        startDate,
        endDate,
        status,
        teams,
        members
    })

    if (project) {
        res.status(201).json({
            _id: project._id,
            business_id: project.business_id,
            name: project.name,
            clientName: project.clientName,
            shortDesc: project.shortDesc,
            startDate: project.startDate,
            endDate: project.endDate,
            status: project.status,
            teams: project.teams,
            members: project.members,
        })
    }

})

// Get all project
const getProjects = asyncHandler(async (req, res) => {
    const projects = await Project.find({ business_id: req.user.business_id }).populate('teams').populate('members')

    if (projects) {
        res.status(200).json(projects)
    }
})

// Get single project
const getProject = asyncHandler(async (req, res) => {
    const project = await Project.find({ business_id: req.user.business_id, _id: req.params.id }).populate('teams').populate('members')

    if (project) {
        res.status(200).json(project)
    }
})

// Update project
const updateProject = asyncHandler(async (req, res) => {
    // Check for validation
    const { errors } = projectUpdateValidate(req.body)
    if (errors) return res.status(400).json(errors)

    const project = await Project.findById(req.params.id)

    if (project) {
        project.name = req.body.name || project.name
        project.clientName = req.body.clientName || project.clientName
        project.shortDesc = req.body.shortDesc || project.shortDesc
        project.startDate = req.body.startDate || project.startDate
        project.endDate = req.body.endDate || project.endDate
        project.status = req.body.status || project.status
        project.teams = req.body.teams || project.teams
        project.members = req.body.members || project.members

        const updatedProject = project.save()

        res.json({
            _id: project._id,
            business_id: project.business_id,
            name: project.name,
            clientName: project.clientName,
            shortDesc: project.shortDesc,
            startDate: project.startDate,
            endDate: project.endDate,
            status: project.status,
            teams: project.teams,
            members: project.members,
        })
    } else {
        res.status(404)
        throw new Error('Project not found')
    }

})

// Delete project
const deleteProject = asyncHandler(async (req, res) => {
    const project = await Project.findById(req.params.id)
    if (project) {
        await project.remove()

        res.json({ message: 'Project removed' })
    } else {
        res.status(404)
        throw new Error('Project not found!')
    }
})

export {
    createProject,
    getProjects,
    getProject,
    updateProject,
    deleteProject
}