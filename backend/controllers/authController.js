import asyncHandler from 'express-async-handler'

import generateToken from '../utils/generateToken.js'
import validateRegisterInput from '../validations/register.js'
import validateLoginInput from '../validations/login.js'
import User from '../models/userModel.js'

// REGISTER USER
const register = asyncHandler(async (req, res) => {
    // Check for validation
    const { errors } = validateRegisterInput(req.body)
    if (errors) return res.status(400).json(errors)
    
    const {name, email, password} = req.body

    const userExist = await User.findOne({email})

    if (userExist){
        res.status(400)
        throw new Error('User already exist')
    }

    const user = await User.create({
        name,
        email,
        password
    })

    if (user){
        res.status(201).json({
            _id: user._id,
            business_id: user.business_id,
            name: user.name,
            email: user.email,
            userType: user.userType,
            token: generateToken(user._id)
        })
    }
    
})

// LOGIN USER
const login = asyncHandler(async(req, res)=>{
    // Check for validation
    const { errors } = validateLoginInput(req.body)
    if (errors) return res.status(400).json(errors)

    const {email, password} = req.body

    const user = await User.findOne({email})
    if (user && (await user.matchPassword(password))){
        res.json({
            _id: user._id,
            business_id: user.business_id,
            name: user.name,
            email: user.email,
            userType: user.userType,
            token: generateToken(user._id)
        })
    }else{
        res.status(401)
        throw new Error('Invalid email or password')
    }

})

export { register, login }