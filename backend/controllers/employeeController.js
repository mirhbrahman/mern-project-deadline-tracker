import asyncHandler from 'express-async-handler'

import generateToken from '../utils/generateToken.js'
import { employeeCreateValidate, employeeUpdateValidate } from '../validations/employee.js'
import User from '../models/userModel.js'

// Create employee
const createEmployee = asyncHandler(async (req, res) => {
    // Check for validation
    const { errors } = employeeCreateValidate(req.body)
    if (errors) return res.status(400).json(errors)

    const { name, email, password, designation } = req.body

    const employeeExist = await User.findOne({ email })

    if (employeeExist) {
        res.status(400)
        throw new Error('Employee already exist')
    }

    const employee = await User.create({
        business_id: req.user.business_id,
        name,
        email,
        password,
        designation,
        userType: 'employee'
    })

    if (employee) {
        res.status(201).json({
            _id: employee._id,
            business_id: employee.business_id,
            name: employee.name,
            email: employee.email,
            userType: employee.employeeType,
            designation: employee.designation,
            token: generateToken(employee._id)
        })
    }

})

// Get all employee
const getEmployees = asyncHandler(async (req, res) => {
    const employees = await User.find({ business_id: req.user.business_id, userType: 'employee' })

    if (employees) {
        res.status(200).json(employees)
    }
})

// Get single employee
const getEmployee = asyncHandler(async (req, res) => {
    const employee = await User.find({ business_id: req.user.business_id, userType: 'employee', _id: req.params.id })

    if (employee) {
        res.status(200).json(employee)
    }
})

// Update employee
const updateEmployee = asyncHandler(async (req, res) => {
    // Check for validation
    const { errors } = employeeUpdateValidate(req.body)
    if (errors) return res.status(400).json(errors)

    const employee = await User.findById(req.params.id)

    if (employee) {
        employee.name = req.body.name || employee.name
        employee.email = req.body.email || employee.email
        employee.designation = req.body.designation || employee.designation
        if (req.body.password) {
            employee.password = req.body.password
        }

        const updatedUser = employee.save()

        res.json({
            _id: employee._id,
            business_id: employee.business_id,
            name: employee.name,
            email: employee.email,
            userType: employee.employeeType,
            designation: employee.designation,
            token: generateToken(employee._id)
        })
    } else {
        res.status(404)
        throw new Error('Employee not found')
    }

})

// Delete employee
const deleteEmployee = asyncHandler(async (req, res) => {
    const employee = await User.findById(req.params.id)
    if (employee) {
        await employee.remove()

        res.json({ message: 'Employee removed' })
    } else {
        res.status(404)
        throw new Error('Employee not found!')
    }
})

export {
    createEmployee,
    getEmployees,
    getEmployee,
    updateEmployee,
    deleteEmployee
}