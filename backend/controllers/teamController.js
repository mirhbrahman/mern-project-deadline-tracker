import asyncHandler from 'express-async-handler'

import { teamCreateValidate, teamUpdateValidate } from '../validations/team.js'
import Team from '../models/teamModel.js'

// Create team
const createTeam = asyncHandler(async (req, res) => {
    // Check for validation
    const { errors } = teamCreateValidate(req.body)
    if (errors) return res.status(400).json(errors)

    const { name, members } = req.body

    const teamExist = await Team.findOne({ name })

    if (teamExist) {
        res.status(400)
        throw new Error('Team already exist')
    }

    const team = await Team.create({
        business_id: req.user.business_id,
        name,
        members
    })

    if (team) {
        res.status(201).json({
            _id: team._id,
            business_id: team.business_id,
            name: team.name,
            members: team.members,
        })
    }

})

// Get all team
const getTeams = asyncHandler(async (req, res) => {
    const teams = await Team.find({ business_id: req.user.business_id }).populate('members')

    if (teams) {
        res.status(200).json(teams)
    }
})

// Get single team
const getTeam = asyncHandler(async (req, res) => {
    const team = await Team.find({ business_id: req.user.business_id, _id: req.params.id }).populate('members')

    if (team) {
        res.status(200).json(team)
    }
})

// Update team
const updateTeam = asyncHandler(async (req, res) => {
    // Check for validation
    const { errors } = teamUpdateValidate(req.body)
    if (errors) return res.status(400).json(errors)

    const team = await Team.findById(req.params.id)

    if (team) {
        team.name = req.body.name || team.name
        team.members = req.body.members || team.members

        const updatedTeam = team.save()

        res.json({
            _id: team._id,
            business_id: team.business_id,
            name: team.name,
            members: team.members,
        })
    } else {
        res.status(404)
        throw new Error('Team not found')
    }

})

// Delete team
const deleteTeam = asyncHandler(async (req, res) => {
    const team = await Team.findById(req.params.id)
    if (team) {
        await team.remove()

        res.json({ message: 'Team removed' })
    } else {
        res.status(404)
        throw new Error('Team not found!')
    }
})

export {
    createTeam,
    getTeams,
    getTeam,
    updateTeam,
    deleteTeam
}