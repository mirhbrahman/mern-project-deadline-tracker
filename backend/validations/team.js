import Joi from "joi"
import generateErrorList from "../utils/generateErrorList.js"

const teamCreateValidate = data => {
    const schema = Joi.object({
        name: Joi.string()
            .min(3)
            .max(50)
            .required(),
        members: Joi.array()
            .required()
    });

    const { error } = schema.validate(data, { abortEarly: false });

    return generateErrorList(error)
};


const teamUpdateValidate = data => {
    const schema = Joi.object({
        name: Joi.string()
            .min(3)
            .max(50)
            .allow(null, ''),
        members: Joi.array()
            .allow(null, ''),
    });

    const { error } = schema.validate(data, { abortEarly: false });

    return generateErrorList(error)
};

export { teamCreateValidate, teamUpdateValidate }