import Joi from "joi"
import generateErrorList from "../utils/generateErrorList.js"

const validate = data => {
    const schema = Joi.object({
        email: Joi.string()
            .min(5)
            .max(255)
            .required()
            .email(),
        password: Joi.string()
            .min(6)
            .max(50)
            .required(),
    });

    const { error } = schema.validate(data, { abortEarly: false })

    return generateErrorList(error)
};

export default validate