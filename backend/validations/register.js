import Joi from "joi"
import generateErrorList from "../utils/generateErrorList.js"

const validate = data => {
    const schema = Joi.object({
        name: Joi.string()
            .min(3)
            .max(50)
            .required(),
        email: Joi.string()
            .min(5)
            .max(255)
            .required()
            .email(),
        password: Joi.string()
            .min(6)
            .max(50)
            .required(),
        passwordConfirm: Joi.any().equal(Joi.ref('password'))
            .required()
            .label('Confirm password')
            .messages({ 'any.only': '{{#label}} does not match' })
    });

    const { error } = schema.validate(data, { abortEarly: false });
   
    return generateErrorList(error)
};

export default validate