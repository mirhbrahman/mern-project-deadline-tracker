import Joi from "joi"
import generateErrorList from "../utils/generateErrorList.js"

const employeeCreateValidate = data => {
    const schema = Joi.object({
        name: Joi.string()
            .min(3)
            .max(50)
            .required(),
        email: Joi.string()
            .min(5)
            .max(255)
            .required()
            .email(),
        password: Joi.string()
            .min(6)
            .max(50)
            .required(),
        passwordConfirm: Joi.any().equal(Joi.ref('password'))
            .required()
            .label('Confirm password')
            .messages({ 'any.only': '{{#label}} does not match' }),
        designation: Joi.string()
            .min(2)
            .max(50)
            .allow(null, ''),
    });

    const { error } = schema.validate(data, { abortEarly: false });
   
    return generateErrorList(error)
};


const employeeUpdateValidate = data => {
    const schema = Joi.object({
        name: Joi.string()
            .min(3)
            .max(50)
            .allow(null, ''),
        email: Joi.string()
            .min(5)
            .max(255)
            .email()
            .allow(null, ''),
        password: Joi.string()
            .min(6)
            .max(50)
            .allow(null, ''),
        passwordConfirm: Joi.any().equal(Joi.ref('password'))
            .label('Confirm password')
            .messages({ 'any.only': '{{#label}} does not match' })
            .allow(null, ''),
        designation: Joi.string()
            .min(2)
            .max(50)
            .allow(null, ''),
    });

    const { error } = schema.validate(data, { abortEarly: false });
   
    return generateErrorList(error)
};

export {employeeCreateValidate, employeeUpdateValidate}