import Joi from "joi"
import generateErrorList from "../utils/generateErrorList.js"

const projectCreateValidate = data => {
    const schema = Joi.object({
        name: Joi.string()
            .min(2)
            .max(50)
            .required(),
        clientName: Joi.string()
            .min(2)
            .max(50)
            .allow(null, ''),
        shortDesc: Joi.string()
            .max(300)
            .allow(null, ''),
        startDate: Joi.date()
            .required(),
        endDate: Joi.date()
            .required(),
        status: Joi.string().required(),
        teams: Joi.array()
            .required(),
        members: Joi.array()
            .required()
    });

    const { error } = schema.validate(data, { abortEarly: false });

    return generateErrorList(error)
};


const projectUpdateValidate = data => {
    const schema = Joi.object({
        name: Joi.string()
            .min(3)
            .max(50)
            .allow(null, ''),
        clientName: Joi.string()
            .min(2)
            .max(50)
            .allow(null, ''),
        shortDesc: Joi.string()
            .max(300)
            .allow(null, ''),
        startDate: Joi.date()
            .allow(null, ''),
        endDate: Joi.date()
            .allow(null, ''),
        status: Joi.string(),
        teams: Joi.array()
            .allow(null, ''),
        members: Joi.array()
            .allow(null, ''),
    });

    const { error } = schema.validate(data, { abortEarly: false });

    return generateErrorList(error)
};

export { projectCreateValidate, projectUpdateValidate }