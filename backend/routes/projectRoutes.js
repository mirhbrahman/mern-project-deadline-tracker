import express from 'express'
const router = express.Router()

import { protect, mainUser } from '../middleware/authMiddleware.js'
import {
    createProject,
    deleteProject,
    getProject,
    getProjects,
    updateProject
} from '../controllers/projectController.js'

router.route('/')
    .get(protect, mainUser, getProjects)
    .post(protect, mainUser, createProject)

router.route('/:id')
    .get(protect, mainUser, getProject)
    .put(protect, mainUser, updateProject)
    .delete(protect, mainUser, deleteProject)


export default router