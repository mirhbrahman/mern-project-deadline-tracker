import express from 'express'
const router = express.Router()

import { protect, mainUser } from '../middleware/authMiddleware.js'
import {
    createEmployee,
    deleteEmployee,
    getEmployee,
    getEmployees,
    updateEmployee
} from '../controllers/employeeController.js'

router.route('/')
    .get(protect, mainUser, getEmployees)
    .post(protect, mainUser, createEmployee)

router.route('/:id')
    .get(protect, mainUser, getEmployee)
    .put(protect, mainUser, updateEmployee)
    .delete(protect, mainUser, deleteEmployee)


export default router