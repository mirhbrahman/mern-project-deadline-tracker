import express from 'express'
const router = express.Router()

import { protect, mainUser } from '../middleware/authMiddleware.js'
import {
    createTeam,
    deleteTeam,
    getTeam,
    getTeams,
    updateTeam
} from '../controllers/teamController.js'

router.route('/')
    .get(protect, mainUser, getTeams)
    .post(protect, mainUser, createTeam)

router.route('/:id')
    .get(protect, mainUser, getTeam)
    .put(protect, mainUser, updateTeam)
    .delete(protect, mainUser, deleteTeam)


export default router