export function errors(state, action) {
  switch (action.type) {
    case "SET_ERRORS":
      return { ...state, errors: action.payload, isError: true };
    case "CLEAR_ERRORS":
      return { ...state, errors: {}, isError: false };
    default:
      return state;
  }
}