export function success(state, action) {
    switch (action.type) {
      case "SET_SUCCESS_MESSAGE":
        return { ...state, successMessage: action.payload };
      default:
        return state;
    }
  }