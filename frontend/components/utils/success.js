import { Context } from "../../context"
import { Alert } from 'react-bootstrap'
import React, { useState, useContext } from "react"

const Success = () => {

    const { state, dispatch } = useContext(Context)

    return (
        <>
            {state.successMessage &&
                <Alert variant="success">
                    <ul>
                        <li>{state.successMessage}</li>
                    </ul>
                </Alert>
            }
        </>
    )
}

export default Success