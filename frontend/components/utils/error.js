import { Context } from "../../context"
import { Alert } from 'react-bootstrap'
import React, { useState, useContext } from "react"

const Error = () => {

    const { state, dispatch } = useContext(Context)

    return (
        <>
        {state.isError &&
            <Alert variant="danger">
                <ul>
                {
                    Object.keys(state.errors).map(function(key, index) {
                       return <li key={index}>{state.errors[key]}</li>
                      })
                }
                </ul>
            </Alert>
        }
        </>
    )
}

export default Error