import Header from './header'
import Footer from './footer'

import { Container } from 'react-bootstrap'

const Layout = ({ children }) => {
    return (
        <Container>
            <Header />
                    <main>{children}</main>
            <Footer />
        </Container>
    )
}

export default Layout