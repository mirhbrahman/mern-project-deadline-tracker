import Link from 'next/link'
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'

import { isAuth } from '../../helpers/helpers'

const Header = () => {
    return (
        <Navbar bg="primary" variant="dark" expand="lg">
            <Container>
                <Link href="/">
                    <a className="navbar-brand">Project Deadline</a>
                </Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav" >
                    {
                        isAuth() ? (
                            <>
                                <Nav className="me-auto">
                                    <Link href="/">
                                        <a className="nav-link">Home</a>
                                    </Link>
                                    <NavDropdown title="Employee" id="basic-nav-dropdown">
                                        <Link href="/employees/create">
                                            <a className="dropdown-item">Create Employee</a>
                                        </Link>
                                        <Link href="/employees">
                                            <a className="dropdown-item">Employee</a>
                                        </Link>
                                    </NavDropdown>

                                    <NavDropdown title="Team" id="basic-nav-dropdown">
                                        <Link href="/teams/create">
                                            <a className="dropdown-item">Create Team</a>
                                        </Link>
                                        <Link href="/teams">
                                            <a className="dropdown-item">Team</a>
                                        </Link>
                                    </NavDropdown>

                                    
                                    <NavDropdown title="Project" id="basic-nav-dropdown">
                                        <Link href="/projects/create">
                                            <a className="dropdown-item">Create Project</a>
                                        </Link>
                                        <Link href="/projects">
                                            <a className="dropdown-item">Project</a>
                                        </Link>
                                    </NavDropdown>
                                </Nav>

                                <Nav>
                                    <Link href="/auth/logout">
                                        <a className="nav-link"><i className="fa fa-sign-out"></i> Logout</a>
                                    </Link>
                                </Nav>
                            </>
                        ) :
                            (
                                <>
                                    <Nav className="me-auto"> </Nav>
                                    <Nav>
                                        <Link href="/auth/login">
                                            <a className="nav-link"><i className="fa fa-sign-in"></i> Login</a>
                                        </Link>
                                        <Link href="/auth/register">
                                            <a className="nav-link"><i className="fa fa-user"></i> Register</a>
                                        </Link>
                                    </Nav>
                                </>
                            )
                    }

                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default Header