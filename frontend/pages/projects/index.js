import { processToken, getAccessToken } from '../../helpers/helpers.js'
import withAuth from '../../middleware/withAuth.js'
import dateFormat from "dateformat"

import React, { useState, useContext } from "react"
import { useRouter } from 'next/router'
import { Context } from "../../context"
import Countdown from 'react-countdown'

import { Table, Button, Image, Badge } from 'react-bootstrap'
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'

export async function getServerSideProps({ req }) {

    let token = req.cookies.accessToken

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/projects', {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let data = await res.json()

    if (!data) {
        return {
            notFound: true,
        }
    }

    return { props: { data } }

}


const Project = ({ data }) => {

    const router = useRouter()

    const { state, dispatch } = useContext(Context)

    // Edit team
    const editProject = async (id) => {
        router.push(`/projects/${id}`)
    }

    // Delete team
    const deleteProject = async (id) => {

        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/projects/' + id, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getAccessToken()
            },
            method: 'DELETE'
        })

        if (res.status == '200') {

            // Dispatch success message
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'Project deleted successfully',
            })
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/projects')
        } else {
            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <>
            <h1>Projects</h1>
            <hr />
            <Success />
            <Error />
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Client Name</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Dadeline</th>
                        <th>Employees</th>
                        <th>Status</th>
                        <th className="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map((item, i) => {
                            return (
                                <tr key={i}>
                                    <td>{++i}</td>
                                    <td>{item.name}</td>
                                    <td>{item.clientName}</td>
                                    <td>{dateFormat(item.startDate, "dd-mm-yyyy")}</td>
                                    <td>{dateFormat(item.endDate, "dd-mm-yyyy")}</td>
                                    <td><Countdown date={item.endDate} /></td>
                                    <td>
                                        {item.members.map((member, j) => {
                                            return (
                                                <li key={j}>{member.name} - {member.designation}</li>
                                            )
                                        })}
                                    </td>
                                    <td>
                                        <>
                                            {item.status == 'pending' ? 
                                            <Badge bg="secondary">{item.status.toUpperCase()}</Badge> : 
                                            <Badge bg="success">{item.status.toUpperCase()}</Badge>
                                            }
                                        </>
                                    </td>
                                    <td className="text-center">
                                        <Button onClick={() => editProject(item._id)} variant="primary" size="sm"><i className="fa fa-edit"></i> Edit</Button>{' '}
                                        <Button onClick={() => deleteProject(item._id)} variant="danger" size="sm"><i className="fa fa-trash"></i> Delete</Button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </Table>
        </>
    )
}

export default withAuth(Project)