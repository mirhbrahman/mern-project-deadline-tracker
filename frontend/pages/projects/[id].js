import { Form, Button, Row, Col } from 'react-bootstrap'
import React, { useState, useContext, useEffect } from "react"
import dateFormat from "dateformat"

import { useRouter } from 'next/router'
import { Context } from "../../context"
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'
import { getAccessToken, processToken } from '../../helpers/helpers.js'
import withAuth from '../../middleware/withAuth'

export async function getServerSideProps({ req, query }) {

    let id = query.id
    let token = req.cookies.accessToken

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/employees', {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let data = await res.json()

    if (!data) {
        return {
            notFound: true,
        }
    }

    const res2 = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/teams', {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let team_data = await res2.json()

    if (!team_data) {
        return {
            notFound: true,
        }
    }

    const res3 = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/projects/' + id, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let old_project = await res3.json()
    old_project = old_project[0]

    if (!old_project) {
        return {
            notFound: true,
        }
    }

    return { props: { data, team_data, old_project } }

}

const EditProject = ({ data, team_data, old_project }) => {
    const router = useRouter()
    let id = router.query.id

    const { state, dispatch } = useContext(Context)
    let old_members_id = old_project.members.map(item=>item._id)
    let old_teams_id = old_project.teams.map(item=>item._id)
  
    const [name, setName] = useState(old_project.name);
    const [clientName, setClientName] = useState(old_project.clientName);
    const [shortDesc, setShortDesc] = useState(old_project.shortDesc);
    const [startDate, setStartDate] = useState(dateFormat(old_project.startDate, 'yyyy-mm-dd'));
    const [endDate, setEndDate] = useState(dateFormat(old_project.endDate, 'yyyy-mm-dd'));
    const [status, setStatus] = useState(old_project.status);
    const [teams, setTeams] = useState(old_teams_id);
    const [members, setMembers] = useState(old_members_id);


    const handleTeams = (event) => {

        let team_id = event.target.value
        // Handle old teams
        let old_team = teams;
        let find_team_index = old_team.findIndex(item => item == team_id);

        if (find_team_index > -1) {
            old_team.splice(find_team_index, 1);
            setTeams([...old_team])
        } else {
            setTeams([...old_team, team_id])
        }

        // Handle old members
        let team = team_data.find(item => item._id == team_id);
        let old_data = members;

        team.members.map(member => {
            let find_index = old_data.findIndex(item => item == member._id);

            if (find_index > -1) {
                old_data.splice(find_index, 1);
                setMembers([...old_data])
            } else {
                setMembers([...old_data, member._id])
            }
        })

    }

    const handleMembers = (event) => {

        let id = event.target.value
        let old_data = members;

        let find_index = old_data.findIndex(item => item == id);

        if (find_index > -1) {
            old_data.splice(find_index, 1);
            setMembers([...old_data])
        } else {
            setMembers([...old_data, id])
        }

    }

    const editProject = async (event) => {

        event.preventDefault()
        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/projects/' + id, {
            body: JSON.stringify({
                name: name,
                clientName: clientName,
                shortDesc: shortDesc,
                startDate: startDate,
                endDate: endDate,
                status: status,
                teams: teams,
                members: members
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getAccessToken()
            },
            method: 'PUT'
        })

        if (res.status == '200') {
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'Project edit successfull',
            })
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/projects')
        } else {
            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <Row className="justify-content-md-center">
            <Col md={8}>
                <h5>Edit Project</h5>
                <hr />

                <Success />
                <Error />

                <Form onSubmit={editProject}>
                    <Form.Group className="mb-3">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" value={name} name="name" onChange={(e) => setName(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Client Name</Form.Label>
                        <Form.Control type="text" value={clientName} name="clientName" onChange={(e) => setClientName(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Short Desc</Form.Label>
                        <Form.Control type="text" value={shortDesc} name="shortDesc" onChange={(e) => setShortDesc(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Start Date</Form.Label>
                        <Form.Control type="date" value={startDate} name="startDate" onChange={(e) => setStartDate(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>End Date</Form.Label>
                        <Form.Control type="date" value={endDate} name="endDate" onChange={(e) => setEndDate(e.target.value)} />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Status</Form.Label>
                        <Form.Select value={status} name="status" onChange={(e) => setStatus(e.target.value)}>
                            <option value="pending">Pending</option>
                            <option value="completed">Completed</option>
                        </Form.Select>
                    </Form.Group>



                    <Form.Group className="mb-3">
                        <Form.Label>Teams</Form.Label>
                        {
                            team_data.map((team, index) => {
                                return (
                                    <Form.Check key={index} checked={teams.includes(team._id)} type="checkbox" value={team._id} label={team.name} onChange={handleTeams} />
                                )
                            })
                        }
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Employees</Form.Label>
                        {
                            data.map((employee, index) => {
                                return (
                                    <Form.Check key={index} checked={members.includes(employee._id)} type="checkbox" checked={members.includes(employee._id)} value={employee._id} label={employee.name} onChange={handleMembers} />
                                )
                            })
                        }
                    </Form.Group>


                    <Button variant="primary" type="submit">
                        Edit
                    </Button>
                </Form>
            </Col>
        </Row>

    )
}


export default withAuth(EditProject)