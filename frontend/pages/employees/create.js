import { Form, Button, Row, Col } from 'react-bootstrap'
import React, { useState, useContext } from "react"
import { useRouter } from 'next/router'

import { Context } from "../../context"
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'
import { getAccessToken } from '../../helpers/helpers.js'
import withAuth from '../../middleware/withAuth'

const CreateEmployee = ()=> {
    const router = useRouter()

    const { state, dispatch } = useContext(Context)

    const createEmployee = async (event) => {
        event.preventDefault()
        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/employees', {
            body: JSON.stringify({
                name: event.target.name.value,
                email: event.target.email.value,
                designation: event.target.designation.value,
                password: event.target.password.value,
                passwordConfirm: event.target.passwordConfirm.value
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getAccessToken()
            },
            method: 'POST'
        })

        if (res.status == '201') {
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'Employee create successfull',
            })
            
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/employees')
        } else {
            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <Row className="justify-content-md-center">
            <Col md={8}>
                <h5>Create Employee</h5>
                <hr />

                <Success />
                <Error />

                <Form onSubmit={createEmployee}>
                    <Form.Group className="mb-3">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" name="name" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" name="email" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Designation</Form.Label>
                        <Form.Control type="text" name="designation" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" name="password" />
                    </Form.Group>
                    <Form.Group className="mb-3" >
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control type="password" name="passwordConfirm" />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Create
                    </Button>
                </Form>
            </Col>
        </Row>

    )
}


export default withAuth(CreateEmployee)