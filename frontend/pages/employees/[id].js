import { Form, Button, Row, Col } from 'react-bootstrap'
import React, { useState, useContext, useEffect } from "react"
import { useRouter } from 'next/router'

import { Context } from "../../context"
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'
import { getAccessToken, processToken } from '../../helpers/helpers.js'
import withAuth from '../../middleware/withAuth'


export async function getServerSideProps({ req, query }) {

    let id = query.id
    let token = req.cookies.accessToken

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/employees/' + id, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let data = await res.json()
    data = data[0]

    if (!data) {
        return {
            notFound: true,
        }
    }

    return { props: { data } }

}

const EditEmployee = ({ data }) => {

    const router = useRouter()
    let id = router.query.id
    const { state, dispatch } = useContext(Context)

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [designation, setDesignation] = useState('')

    useEffect(() => {
        setName(data.name)
        setEmail(data.email)
        setDesignation(data.designation)
    }, [])

    const editEmployee = async (event) => {
        event.preventDefault()
        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/employees/' + id, {
            body: JSON.stringify({
                name: event.target.name.value,
                email: event.target.email.value,
                designation: event.target.designation.value,
                password: event.target.password.value,
                passwordConfirm: event.target.passwordConfirm.value
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getAccessToken()
            },
            method: 'PUT'
        })

        if (res.status == '200') {
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'Employee update successfull',
            })
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/employees')
        } else {
            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <Row className="justify-content-md-center">
            <Col md={8}>
                <h5>Edit Employee</h5>
                <hr />

                <Success />
                <Error />

                <Form onSubmit={editEmployee}>
                    <Form.Group className="mb-3">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" name="name" value={name} onChange={(e) => setName(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" name="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Designation</Form.Label>
                        <Form.Control type="text" name="designation" value={designation} onChange={(e) => setDesignation(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" name="password" />
                    </Form.Group>
                    <Form.Group className="mb-3" >
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control type="password" name="passwordConfirm" />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Update
                    </Button>
                </Form>
            </Col>
        </Row>

    )
}


export default withAuth(EditEmployee)