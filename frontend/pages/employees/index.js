import { processToken, getAccessToken } from '../../helpers/helpers.js'
import withAuth from '../../middleware/withAuth.js'

import React, { useState, useContext } from "react"
import { useRouter } from 'next/router'
import { Context } from "../../context"

import { Table, Button, Image } from 'react-bootstrap'
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'

export async function getServerSideProps({ req }) {

    let token = req.cookies.accessToken

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/employees', {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let data = await res.json()

    if (!data) {
        return {
            notFound: true,
        }
    }

    return { props: { data } }

}


const Employee = ({ data }) => {

    const router = useRouter()

    const { state, dispatch } = useContext(Context)

    // Edit employee
    const editEmployee = async(id)=>{
        router.push(`/employees/${id}`)
    }

    // Delete employee
    const deleteEmployee = async (id) => {

        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/employees/' + id, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getAccessToken()
            },
            method: 'DELETE'
        })

        if (res.status == '200') {

            // Dispatch success message
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'Employee deleted successfully',
            })
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/employees')
        } else {
            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <>
            <h1>Employee</h1>
            <hr />
            <Success />
            <Error />
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Designation</th>
                        <th className="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map((item, i) => {
                            return (
                                <tr key={i}>
                                    <td>{++i}</td>
                                    <td>{item.name}</td>
                                    <td>{item.email}</td>
                                    <td>{item.designation}</td>
                                    <td className="text-center">
                                        <Button onClick={() => editEmployee(item._id)} variant="primary" size="sm"><i className="fa fa-edit"></i> Edit</Button>{' '}
                                        <Button onClick={() => deleteEmployee(item._id)} variant="danger" size="sm"><i className="fa fa-trash"></i> Delete</Button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </Table>
        </>
    )
}

export default withAuth(Employee)