import { Form, Button, Row, Col } from 'react-bootstrap'
import React, { useState, useContext } from "react"

import { useRouter } from 'next/router'
import { Context } from "../../context"
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'
import { getAccessToken, processToken } from '../../helpers/helpers.js'
import withAuth from '../../middleware/withAuth'

export async function getServerSideProps({ req }) {

    let token = req.cookies.accessToken

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/employees', {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let data = await res.json()

    if (!data) {
        return {
            notFound: true,
        }
    }

    return { props: { data } }

}

const CreateTeam = ({ data }) => {
    const router = useRouter()

    const { state, dispatch } = useContext(Context)

    const [name, setName] = useState('');
    const [members, setMembers] = useState([]);


    const handleMembers = (event) => {

        let id = event.target.value
        let old_data = members;

        let find_index = old_data.findIndex(item => item == id);

        if (find_index > -1) {
            old_data.splice(find_index, 1);
            setMembers([...old_data])
        } else {
            setMembers([...old_data, id])
        }

    }

    const createTeam = async (event) => {

        event.preventDefault()
        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/teams', {
            body: JSON.stringify({
                name: name,
                members: members
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getAccessToken()
            },
            method: 'POST'
        })

        if (res.status == '201') {
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'Team create successfull',
            })
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/teams')
        } else {
            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <Row className="justify-content-md-center">
            <Col md={8}>
                <h5>Create Team</h5>
                <hr />

                <Success />
                <Error />

                <Form onSubmit={createTeam}>
                    <Form.Group className="mb-3">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" name="name" onChange={(e) => setName(e.target.value)} />
                    </Form.Group>

                    {
                        data.map((employee, index) => {
                            return (
                                <Form.Group className="mb-3" key={index}>
                                    <Form.Check type="checkbox" value={employee._id} label={employee.name} onChange={handleMembers} />
                                </Form.Group>
                            )
                        })
                    }


                    <Button variant="primary" type="submit">
                        Create
                    </Button>
                </Form>
            </Col>
        </Row>

    )
}


export default withAuth(CreateTeam)