import { Form, Button, Row, Col } from 'react-bootstrap'
import React, { useState, useContext, useEffect } from "react"

import { useRouter } from 'next/router'
import { Context } from "../../context"
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'
import { getAccessToken, processToken } from '../../helpers/helpers.js'
import withAuth from '../../middleware/withAuth'

export async function getServerSideProps({ req, query }) {

    let id = query.id
    let token = req.cookies.accessToken

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/employees', {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let data = await res.json()

    if (!data) {
        return {
            notFound: true,
        }
    }

    const res2 = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/teams/' + id, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let team_data = await res2.json()
    team_data = team_data[0]

    if (!team_data) {
        return {
            notFound: true,
        }
    }

    return { props: { data, team_data } }

}

const EditTeam = ({ data, team_data }) => {
    const router = useRouter()
    let id = router.query.id
    const { state, dispatch } = useContext(Context)
    let old_members_ids = team_data.members.map(item=>item._id)

    const [name, setName] = useState('');
    const [members, setMembers] = useState(old_members_ids);


    useEffect(()=>{
        setName(team_data.name)
    },[])


    const handleMembers = (event) => {

        let id = event.target.value
        let old_data = members;

        let find_index = old_data.findIndex(item => item == id);

        if (find_index > -1) {
            old_data.splice(find_index, 1);
            setMembers([...old_data])
        } else {
            setMembers([...old_data, id])
        }

    }

    const editTeam = async (event) => {

        event.preventDefault()
        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/teams/' + id, {
            body: JSON.stringify({
                name: name,
                members: members
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getAccessToken()
            },
            method: 'PUT'
        })

        if (res.status == '200') {
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'Team update successfull',
            })
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/teams')
        } else {
            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <Row className="justify-content-md-center">
            <Col md={8}>
                <h5>Edit Team</h5>
                <hr />

                <Success />
                <Error />

                <Form onSubmit={editTeam}>
                    <Form.Group className="mb-3">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" value={name} name="name" onChange={(e) => setName(e.target.value)} />
                    </Form.Group>

                    {
                        data.map((employee, index) => {
                            return (
                                <Form.Group className="mb-3" key={index}>
                                    <Form.Check type="checkbox" checked={members.includes(employee._id)} value={employee._id} label={employee.name} onChange={handleMembers} />
                                </Form.Group>
                            )
                        })
                    }


                    <Button variant="primary" type="submit">
                        Edit
                    </Button>
                </Form>
            </Col>
        </Row>

    )
}


export default withAuth(EditTeam)