import { processToken, getAccessToken } from '../../helpers/helpers.js'
import withAuth from '../../middleware/withAuth.js'

import React, { useState, useContext } from "react"
import { useRouter } from 'next/router'
import { Context } from "../../context"

import { Table, Button, Image } from 'react-bootstrap'
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'

export async function getServerSideProps({ req }) {

    let token = req.cookies.accessToken

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_BASE}` + '/api/teams', {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': processToken(token)
        },
        method: 'GET'
    })

    let data = await res.json()

    if (!data) {
        return {
            notFound: true,
        }
    }

    return { props: { data } }

}


const Team = ({ data }) => {
   
    const router = useRouter()

    const { state, dispatch } = useContext(Context)

    // Edit team
    const editTeam = async(id)=>{
        router.push(`/teams/${id}`)
    }

    // Delete team
    const deleteTeam = async (id) => {

        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/teams/' + id, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getAccessToken()
            },
            method: 'DELETE'
        })

        if (res.status == '200') {

            // Dispatch success message
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'Team deleted successfully',
            })
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/teams')
        } else {
            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <>
            <h1>Teams</h1>
            <hr />
            <Success />
            <Error />
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Employees</th>
                        <th className="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map((item, i) => {
                            return (
                                <tr key={i}>
                                    <td>{++i}</td>
                                    <td>{item.name}</td>
                                    <td>
                                        {item.members.map((member, j)=>{
                                            return (
                                                <li key={j}>{member.name} - {member.designation}</li>
                                            )
                                        })}
                                    </td>
                                    <td className="text-center">
                                        <Button onClick={() => editTeam(item._id)} variant="primary" size="sm"><i className="fa fa-edit"></i> Edit</Button>{' '}
                                        <Button onClick={() => deleteTeam(item._id)} variant="danger" size="sm"><i className="fa fa-trash"></i> Delete</Button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </Table>
        </>
    )
}

export default withAuth(Team)