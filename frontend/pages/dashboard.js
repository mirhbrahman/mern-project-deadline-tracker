import withAuth from '../middleware/withAuth.js'

const Dashboard = () => {
    return (
        <h1>Dashboard</h1>
    )
}

export default withAuth(Dashboard)