import 'bootstrap/dist/css/bootstrap.min.css'
import { Provider2 } from "../context";
import Layout from '../components/layouts/layout'
import '../styles/globals.css'
import '../public/font-awesome.min.css'


function MyApp({ Component, pageProps }) {
  return (
    <Provider2>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider2>
  )
}

export default MyApp
