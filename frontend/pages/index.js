import Layout from '../components/layouts/layout'
import Success from '../components/utils/success'

const Home = () => {
  return (
    <>
      <h1>Home</h1>
      <Success />
    </>
  )
}

export default Home
