import { Form, Button, Row, Col } from 'react-bootstrap'
import React, { useState, useContext } from "react"
import Cookies from 'js-cookie'
import { useRouter } from 'next/router'
import { isAuth } from '../../helpers/helpers'

import { Context } from "../../context"
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'

export default function login() {
    const router = useRouter()

    // Check auth
    if(isAuth()){
        router.push('/dashboard')
    }

    const { state, dispatch } = useContext(Context)

    const loginUser = async (event) => {
        event.preventDefault()
        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/auth/login', {
            body: JSON.stringify({
                email: event.target.email.value,
                password: event.target.password.value
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        })

        if (res.status == '200') {
            // Store token to cookies
            let data = await res.json();
            if(data.token){
                Cookies.set('accessToken', data.token)
            }

            // Dispatch success message
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'User login successfull',
            })
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/dashboard')
        } else {
            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <Row className="justify-content-md-center">
            <Col md={8}>
                <h5>Login</h5>
                <hr />

                <Success />
                <Error />

                <Form onSubmit={loginUser}>
                   <Form.Group className="mb-3">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" name="email" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" name="password" />
                    </Form.Group>
                 
                    <Button variant="primary" type="submit">
                        Login
                    </Button>
                </Form>
            </Col>
        </Row>

    )
}