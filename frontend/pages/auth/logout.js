import { useRouter } from 'next/router'
import { removeToken } from '../../helpers/helpers'

const Logout = () => {
    const router = useRouter()

    // Check auth
    if(typeof window !== 'undefined'){
        removeToken()
        router.push('/')
    }

    return (
        <></>
    )
}

export default Logout