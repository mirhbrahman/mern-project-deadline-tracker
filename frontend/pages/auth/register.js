import { Form, Button, Row, Col } from 'react-bootstrap'
import React, { useState, useContext } from "react"
import { useRouter } from 'next/router'

import { Context } from "../../context"
import Error from '../../components/utils/error'
import Success from '../../components/utils/success'

export default function register() {
    const router = useRouter()
    // const [isError, setIsError] = useState(false)
    // const [errors, setErrors] = useState({})

    const { state, dispatch } = useContext(Context)

    const registerUser = async (event) => {
        event.preventDefault()
        const res = await fetch(process.env.NEXT_PUBLIC_API_BASE + '/api/auth/register', {
            body: JSON.stringify({
                name: event.target.name.value,
                email: event.target.email.value,
                password: event.target.password.value,
                passwordConfirm: event.target.passwordConfirm.value
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        })

        if (res.status == '201') {
            dispatch({
                type: "SET_SUCCESS_MESSAGE",
                payload: 'User registration successfull',
            })
            dispatch({
                type: "CLEAR_ERRORS"
            })
            router.push('/')
        } else {
            // setIsError(true)
            // setErrors(await res.json())

            dispatch({
                type: "SET_ERRORS",
                payload: await res.json(),
            })
        }
    }

    return (
        <Row className="justify-content-md-center">
            <Col md={8}>
                <h5>Register</h5>
                <hr />

                <Success />
                <Error />

                <Form onSubmit={registerUser}>
                    <Form.Group className="mb-3">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" name="name" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" name="email" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" name="password" />
                    </Form.Group>
                    <Form.Group className="mb-3" >
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control type="password" name="passwordConfirm" />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Register
                    </Button>
                </Form>
            </Col>
        </Row>

    )
}