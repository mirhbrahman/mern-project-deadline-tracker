import Cookies from 'js-cookie'

const isAuth = () => {
    let token = Cookies.get('accessToken')

    if (token) {
        return true
    } else {
        return false
    }
}


const getAccessToken = () => {
    let token = Cookies.get('accessToken')

    if (token) {
        return `Bearer ${token}`
    } else {
        return false
    }
}

const processToken = (token) => {
    return `Bearer ${token}`
}

const removeToken = () => {
    Cookies.remove('accessToken')
}

export { isAuth, getAccessToken, processToken, removeToken }